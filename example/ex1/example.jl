
# example of a cyclic proposal
# Restrict to torus by appling modulo [a,b] to proposed value  
cycle(t,a,b) = @. mod(t - a, b - a) + a
CyclicWalk(a,b,s) = @. TransformedProposal(t->cycle(t,a,b), RandomWalk(Uniform(a,b), Uniform(-s,s)) )

c = CyclicWalk(-1,1, .1)