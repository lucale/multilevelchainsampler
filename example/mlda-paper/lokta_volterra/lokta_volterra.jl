using Parameters 
using Statistics
using OrdinaryDiffEq
using CairoMakie


@kwdef struct LoktaVolterra{R <: AbstractFloat}
    a::R = 3.0  
    b::R = 0.7
    c::R = 0.2   
    d::R = 1.0
    P0::R = 10.0
    Q0::R = 5.0
    T::R = 10.0
end

function ode_dynamcis!(du, u, lv::LoktaVolterra, t)
    @unpack a, b, c, d = lv
    du[1] =  a * u[1] - b * u[1] * u[2]
    du[2] = -d * u[2] + c * u[1] * u[2]
    return du
end

function ode_problem(lv::LoktaVolterra)
    @unpack P0,Q0,T = lv
    prob = ODEProblem(ode_dynamcis!, [P0, Q0], (0.0, T), lv)
    return prob
end


lv = LoktaVolterra()
prob = ode_problem(lv)
@time sol = solve(prob, RK4(), adaptive=true, reltol=1e-6)
P = getindex.(sol.u,1)
Q = getindex.(sol.u,2)
M = mean.([P,Q])



begin
    t = sol.t
    fig = Figure()
    ax = Axis(fig[1, 1])
    l = lines!(ax, t, P, label="P")
    lines!(ax, t, M[1] .* ones(length(t)), linestyle=:dash, color = l.color)
    l = lines!(ax, t, Q, label="Q")
    lines!(ax, t, M[2] .* ones(length(t)), linestyle=:dash, color = l.color)
    axislegend(ax)
    save("$(@__DIR__)/plots/lokta_volterra.png", fig)
    display(fig)
end

