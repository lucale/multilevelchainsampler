using Distributions
using MultilevelChainSampler
using LinearAlgebra

struct BayesModel{F, D} 
    forward :: F
    data :: D
end
BayesModel(f::Function, d) = BayesModel{typeof(f), typeof(d)}(f,d)
function BayesModel(f::Function, θ_true, noise)
    d_true = f(θ_true)
    d = d_true + noise * randn(size(d_true)...)
    return BayesModel{typeof(f), typeof(d)}(f, d)
end
forward(m::BayesModel, θ) = m.forward(θ)
(m::BayesModel)(θ) = forward(m, θ)

function Distributions.logpdf(m::BayesModel, θ)
    Δd = m(θ) - m.data 
    return -.5 * sum(Δd.^2)
end
LogDensity(m::BayesModel) = LogDensity(θ -> logpdf(m, θ))



#g = construct_2d_grid()
using CairoMakie


function construct_2d_grid(n::Integer)
    t = [ 0:1/(n-1):1 ... ] .* 2 .- 1
    x = repeat(t, inner=n)
    y = repeat(t, outer=n)
    z = hcat(x, y) 
    z = reshape(z, n, n, 2)
end

n = 100; m = 100
grid_param  = construct_2d_grid(n)
grid_colloc = construct_2d_grid(m)

t = reshape(grid_param,  n^2,2)
s = reshape(grid_colloc, m^2,2)
F = (s[:,1]' .- t[:,1]).^2 .+ (s[:,2]' .- t[:,2]).^2 
F = 1 ./ F  .* 0.1 / (4*m^2)
forward(f) = reshape( F * reshape(f, m^2), n, n)

f_true = map( 
    (x,y) -> 1.0 * ( x^2 + y^2 < 1/2),
    grid_param[:,:,1], grid_param[:,:,2]
)
#f_grid = f_true .+ randn(size(f_true)...) * 0.1

model = BayesModel(forward, forward(f_true))
prop = MvNormal(zeros(n^2), I)
prop = RandomWalk(prop, prop)
prop = TransformedProposal(x->reshape(x, n,n), prop)
mh = MetropolisHastings(prop)

begin
    fig = Figure(size=(1100, 500))
    
    ax = Axis(fig[1,1])
    c = heatmap!(ax, f_true, colormap=:plasma)
    Colorbar(fig[1,2], c)
    tightlimits!(ax)
    ax.title = "True density"
    
    ax = Axis(fig[1,3])
    c = heatmap!(ax, model.data, colormap=:plasma)
    Colorbar(fig[1,4], c)
    tightlimits!(ax)
    ax.title = "Noisy signal"  

    display(fig)
end