include("utils.jl")

using MultilevelChainSampler
using CairoMakie



# =============================================================================
#  Test KL expansion on square grid

n = 32
x = grid2d(n, n)
m = Matern(1.0, 0.1, 3/2)
C = cov(m, x)
k = KarhunenLoeve(C)

function plot_square!(ax::Axis, f::Vector; kwargs...)
    n = convert(Int, sqrt(length(f)))
    f = reshape(f, n, n)
    heatmap!(ax, f, colormap=:plasma; kwargs...)
    #contourf!(ax, f, colormap=:plasma; kwargs...)
end

# plot basis mode 
begin 
    nplt = (8,8)
    fig = Figure(size=nplt.*(320, 320))
    for i = 1:nplt[1]
        for j = 1:nplt[2]
            ax = Axis(fig[i,2j-1])

            # basis vector of Karhunen-Loeve expansion
            idx = (i-1)*nplt[2] + j
            f = k.V[:,idx]

            plt = plot_square!(ax, f)
            Colorbar(fig[i,2j], plt)
            ax.title = "mode $idx"
        end
    end
    save("$(@__DIR__)/plots/basis_modes.png", fig)
    display(fig)
end


begin
    nsamp = 5
    levels = [ reverse([4, 32, 64, 256, 1024]) ... ]

    fig = Figure(size=(nsamp, length(levels)).*(320, 320))
    for j=1:nsamp

        # sample from normal
        θ = k.λ .* randn(length(k))  

        for (i,n) in enumerate(levels)

            f = transform(k,  θ[1:n])

            ax = Axis(fig[i,2*j-1])
            plt = plot_square!(ax, f)
            Colorbar(fig[i,2*j], plt)
            ax.title = "sample # $j: level $n "
        end
    end
    save("$(@__DIR__)/plots/multilevel_sample.png", fig)
    display(fig)
end

