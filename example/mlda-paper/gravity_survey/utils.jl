using Random
using Distributions
using LinearAlgebra
using SpecialFunctions
using Parameters


# spatial coordinates for a 2D grid
function grid2d(n=10,m=10, a=0, b=1) 
    x1 = [0:1/(n-1):1 ... ]
    x2 = [0:1/(m-1):1 ... ]
    x1 = repeat(x1, inner=m)
    x2 = repeat(x2, outer=n)
    x = hcat(x1,x2)
    x = @. a + (b-a) * x
    return x
end

function midpoints2d(x::Matrix, shape::Tuple{<:Integer, <:Integer})
    x = reshape(x, (shape..., 2) )
    
    #h1 = x[2:end,:,1] - x[1:end-1,:,1]
    #h2 = x[:,2:end,2] - x[:,1:end-1,2]
    #h = hcat(h1,h2)
    #h = reshape(h, prod(size(h)[1:end-1]), 2)

    m1 = @. (x[1:end-1,:,:] + x[2:end,:,:]) / 2
    m1 = reshape(m1, prod(size(m1)[1:end-1]), 2)

    m2 = @. (x[:,1:end-1,:] + x[:,2:end,:]) / 2
    m2 = reshape(m2, prod(size(m2)[1:end-1]), 2)

    m = vcat(m1,m2)

    return m
end

function eval_midpoints2d(f::Vector, shape::Tuple{<:Integer, <:Integer})
    f = reshape(f, shape)
    m1 = @. (f[1:end-1,:] + f[2:end,:]) / 2
    m1 = reshape(m1, prod(size(m1)))
    m2 = @. (f[:,1:end-1] + f[:,2:end]) / 2
    m2 = reshape(m2, prod(size(m2)))
    m = vcat(m1,m2)
    return m
end


# per point distance of two sets of points
function distance_matrix(x::Matrix, y::Matrix)
    @assert size(x,2) == size(y,2)
    d = size(x, 2)
    D = zeros(size(x,1), size(y,1))
    for i=1:d
        D .+= ( x[:,i] .- y[:,i]' ) .^2
    end
    sqrt.(D)
end

# matern kernel
struct Matern{R <: Real}
    σ::R
    ρ::R
    ν::R
end
function Matern(σ = 1.0,ρ=.2,ν=3/2) 
    T = promote_type(typeof.((σ,ρ,ν))...)
    return Matern{T}(σ, ρ, ν)
end

function cov(m::Matern, d::Real)
    @unpack σ, ρ, ν = m
    
    d = d < eps() ? eps() : d  # If d is zero, besselk will return NaN
    d = sqrt( 2 * ν ) * d / ρ
    c = σ^2 * 2^(1-ν)/gamma(ν) * (ρ*d)^ν * besselk(ν,d)
    return c
end

# construct covariance matrix from a spatial discretization
function cov(m::Matern, x::Matrix) 
    D = distance_matrix(x, x)
    C = cov.((m,), D)    
end

# principal component analysis
struct KarhunenLoeve
    λ::Vector{Float64}
    V::Matrix{Float64}
end
Base.length(k::KarhunenLoeve) = length(k.λ)
function KarhunenLoeve(C::Matrix{Float64})
    λ,V = eigen(C,sortby=x->-abs(x))
    λ = real.(λ)
    λ = sqrt.(abs.(λ))
    return KarhunenLoeve(λ, V)
end
transform(k::KarhunenLoeve, θ::Vector) = k.V[:,1:length(θ)] * θ
project(k::KarhunenLoeve, x::Vector, dims::Int=length(k)) = k.V'[1:dims,:] * x 
(k::KarhunenLoeve)(θ) = transform(k,θ)



