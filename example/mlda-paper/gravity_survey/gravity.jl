include("utils.jl")

using CairoMakie

using Revise
using MultilevelChainSampler


function plot_grid!(ax::Axis, f::Vector, x::Matrix; kwargs...)
    scatter!(ax, x[:,1], x[:,2], color=f, colormap=:plasma; kwargs...)
end

size_x = (32,32)
size_y = (32,32)
x = grid2d(size_x ... )          # collocation points (data)
y = grid2d(size_y ... )          # sample space points (unknown)

qy = midpoints2d(y,size_y) # quadrature points 


# build the matrix A
depth = 0.2
D = distance_matrix(x,qy)
A = depth * ( 1 ./ D.^3 ) * 1/32^2

circle_indicator(x,y,a=0,b=0,r=1) = ((x-a)^2 + (y-b)^2 < r^2 )
f = 1.0 * circle_indicator.(y[:,1], y[:,2], 0.5, 0.5, 0.2)
g = A * eval_midpoints2d(f, size_y )

# Naive sampling 
#prop = RandomWalk(MvNormal(zeros(length(f)), I), MvNormal(zeros(length(f)), .5 * I))
prop = RandomWalk(Normal(), Normal(0,0.1))
prop = ComponentwiseProposal(prop,length(f))
mh = MetropolisHastings(prop,true)
t = LogDensity(f -> - 1e-3 * sum( (g - A * eval_midpoints2d(f,size_y)) .^ 2 ) )

f0 = f + 0.1 * randn(length(f))

print("Sampling naively")
@time c = sample(t, mh, 10000)
c_x = getfield.(c, :x)
c_lp = getfield.(c, :logp_x)

i_best = argmax(lp[1000:end]) + 999

fig = Figure(size=(1200, 600))
ax = Axis(fig[1,3]); plt = lines!(ax, 1:length(c_lp), -c_lp); ax.xlabel = "time"; ax.ylabel = "energy"
ax = Axis(fig[1,1]); plt = plot_grid!(ax, f, y); Colorbar(fig[1,2], plt)
ax = Axis(fig[2,1]); plt = plot_grid!(ax, c_x[1], y); Colorbar(fig[2,2], plt); ax.title = "initial i=1"
ax = Axis(fig[2,3]); plt = plot_grid!(ax, c_x[i_best], y); Colorbar(fig[2,4], plt); ax.title="at i=$i_best"
ax = Axis(fig[2,5]); plt = plot_grid!(ax, c_x[end], y); Colorbar(fig[2,6], plt); ax.title="final i=$(length(c))"
save("$(@__DIR__)/../plots/chain_naive.png", fig)
display(fig)


# Sampling in KL space
print("Setting up KL space ")
@time begin 
    M = Matern()
    C = cov(M, x)
    k = KarhunenLoeve(C)
    θ_true = project(k, f)
end

prop = RandomWalk(Normal(), Normal(0,0.1))
prop = ComponentwiseProposal(prop, 256)
mh = MetropolisHastings(prop, true)
θ0 = project(k, f0, 512)
print("Sampling in KL space")
t = LogDensity( θ -> sum( -( A * eval_midpoints2d( transform(k, θ), size_y) - g ).^2 ) )

@time c = sample(t, mh, 10000)
c_x = getfield.(c, :x)
c_x = transform.([k], c_x)
c_lp = getfield.(c, :logp_x)

i_best = argmax(lp[1000:end]) + 999

fig = Figure(size=(1200, 600))
ax = Axis(fig[1,3]); plt = lines!(ax, 1:length(c_lp), -c_lp); ax.xlabel = "time"; ax.ylabel = "energy"
ax = Axis(fig[1,1]); plt = plot_grid!(ax, f, y); Colorbar(fig[1,2], plt)
ax = Axis(fig[2,1]); plt = plot_grid!(ax, c_x[1], y); Colorbar(fig[2,2], plt); ax.title = "initial i=1"
ax = Axis(fig[2,3]); plt = plot_grid!(ax, c_x[i_best], y); Colorbar(fig[2,4], plt); ax.title="at i=$i_best"
ax = Axis(fig[2,5]); plt = plot_grid!(ax, c_x[end], y); Colorbar(fig[2,6], plt); ax.title="final i=$(length(c))"
save("$(@__DIR__)/../plots/chain_naive.png", fig)
display(fig)



# Sampling in KL space with DA
prop = RandomWalk(Normal(), Normal(0,0.1))
prop = MultiProposal(
    ComponentwiseProposal(prop, 8),
    ComponentwiseProposal(prop, 64),
    ComponentwiseProposal(prop, 256),
    ComponentwiseProposal(prop, 1042 - 8 - 64 - 256)
)

da = MLDA(prop, (8,8,8), true)


t = LogDensity( θ -> sum( -( A * eval_midpoints2d( transform(k, vcat(collect.(θ)...)), size_y) - g ).^2 ) )
print("Sampling delayed acceptance")
@time c = sample(t, da, 100; chain_type=MultilevelChains)

c_x = getfield.(c, :x)
c_x = transform.([k], c_x)
c_lp = getfield.(c, :logp_x)


i_best = argmax(last.(c[end][1000:end])) + 999
fig = Figure(size=(1200, 600))
ax = Axis(fig[1,3]); plt = lines!(ax, 1:length(c_x), c_lp); ax.xlabel = "time"; ax.ylabel = "log probability"
ax = Axis(fig[1,1]); plt = plot_grid!(ax, f, y); Colorbar(fig[1,2], plt)
ax = Axis(fig[2,1]); plt = plot_grid!(ax, c_x[1], y); Colorbar(fig[2,2], plt); ax.title = "initial i=1"
ax = Axis(fig[2,3]); plt = plot_grid!(ax, c_x[i_best], y); Colorbar(fig[2,4], plt); ax.title="at i=$i_best"
ax = Axis(fig[2,5]); plt = plot_grid!(ax, c_x[end], y); Colorbar(fig[2,6], plt); ax.title="final i=$(length(c))"
save("$(@__DIR__)/../plots/chain_KLspace_da.png", fig)
display(fig)
