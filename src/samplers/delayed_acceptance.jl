
using AbstractMCMC
using Distributions
using Random
using BangBang

"""
    MLDA(p::Vector{<:AbstractProposal}, n::Vector{<:Union{Integer,Distribution}}; 
        save_logprob=false, save_reject=false, save_subchain=false)


Construct a multilevel delayed acceptance sampler 
from proposal vector `p` and subchain length vector `n`.
They can either be fixed integers or distributions that are sampled at each iteration.

- `save_logprob` : saves target evaluations 

- `save_reject` : saves rejection status

- `save_subchain` : saves subchain samples 

"""
struct MLDA{P <: AbstractProposal, N <:Union{Integer,Distribution}, save_logprob, save_reject, save_subchain} <: AbstractMCMC.AbstractSampler
    p :: Vector{P} # multilevel proposal 
    n :: Vector{N} # subchain lengths
end

# Constructor
function MLDA(p::Vector{<:AbstractProposal}, n::Vector{<:Union{Integer,Distribution}};
    save_logprob=false, save_reject=false, save_subchain=false) 

    if length(p) == 1 
        warn("Single level MLDA reduced to MH")
        return MH(p[1], save_logprob, save_reject) 
    end

    # check if number of subchain lengths is correct
    if length(n) != length(p)-1
        throw(ArgumentError("Number of subchain lengths $(length(n)) must be $(length(p)-1) "))
    end

    MLDA{eltype(p), eltype(n), save_logprob, save_reject, save_subchain}(p, n)
end


# Save options
saves_logprob(da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P,N,save_logprob,save_reject,save_subchain} = save_logprob
saves_reject(da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P,N,save_logprob,save_reject,save_subchain} = save_reject
saves_subchain(da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P,N,save_logprob,save_reject,save_subchain} = save_subchain

# Check if subchain has fixed size 
has_fixed_length(::MLDA) = false
has_fixed_length(da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P, N <:Integer,save_logprob,save_reject,save_subchain} = true

# Expected size of output, used in size hint
function Base.size(da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P, N <:Integer,save_logprob,save_reject,save_subchain} 
    return (reverse(cumprod(reverse(da.n))) ... , 1)
end

function AbstractMCMC.step(rng::AbstractRNG, model::AbstractLogDensity, da::MLDA; x0=nothing, logp_x0=nothing, kwargs...)

    #supports_multilevel(model) || throw(ArgumentError("Model does not support multilevel sampling."))
    
    # start from initial state
    if isnothing(x0)
        x0 = tuple( propose.([rng], da.p) ... )
        logp_x0 = Tuple( evaluate(model, x0[1:l]) for l=1:length(x0) )
    elseif isnothing(logp_x0)
        logp_x0 = Tuple( evaluate(model, x0[1:l]) for l=1:length(x0) )
    end

    # promote initial state level to finest sampler level
    if length(x0) < length(da.p)
        x0 = (x0 ... , propose.(rng, da.p.proposals[length(x0)+1:end])  ... )
        logp_x0 = (logp_x0 ... ,  ( evaluate.(model, x0[1:i]) for i=length(x0)+1:length(da.p) )...  )
    end

    # build initial sampling state 
    subchain = [ Vector{Tuple{typeof(x0[i]), typeof(logp_x0[i]), Bool}}(undef,0) for i=1:length(x0)-1 ]
    sample = (x0, logp_x0, true, 0, subchain)
    state = sample[1:2]
    return sample, state 
end

subchain_length(rng::AbstractRNG, da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P, N<:Integer,save_logprob,save_reject,save_subchain} = da.n[end]
subchain_length(rng::AbstractRNG, da::MLDA{P,N,save_logprob,save_reject,save_subchain}) where {P, N<:Distribution,save_logprob,save_reject,save_subchain} = rand(rng, da.n[end])

function sample_subchain(rng::AbstractRNG, model::AbstractLogDensity, da::MLDA,x, logp_x)
    n = subchain_length(rng, da)

    if length(da.p) == 2
        subsampler = MH(da.p[1]; save_logprob=true, save_reject=true)
        submodel = LogDensity( x -> evaluate(model, (x,)) )
        subchain = sample(rng, submodel, subsampler, n; 
                          x0=x[1], logp_x0=logp_x[1], discard_initial=1, progress=false)
        subchain = [subchain]
    else 
        subsampler = MLDA( da.p[1:end-1], da.n[1:end-1]; save_logprob=true, save_reject=true, save_subchain=true)
        subchain = sample(rng, model, subsampler, n; 
            x0=x[1:end-1], logp_x0=logp_x[1:end-1], discard_initial=1, progress=false
            #chain_type = Tuple,  # TODO: more specific?
        )
    end

    return n, subchain
end


function AbstractMCMC.step(rng::AbstractRNG, model::AbstractLogDensity, da::MLDA, state; kwargs...)
    x, logp_x = state

    # create subchain recursively 
    n, subchain = sample_subchain(rng, model, da, x, logp_x)
            
    # proposed sample is final sample of subchain 
    y_course      = getindex.(last.(subchain), 1)
    logp_y_course = getindex.(last.(subchain), 2)
    acc_course = getindex.(last.(subchain), 3)
    subchain .= map(x->x[1:end-1], subchain)

    # combine with fine state proposal
    y_fine = propose(da.p[end], x[end])
    y = (y_course..., y_fine)
    logp_y_fine = evaluate(model, y)
    logp_y = (logp_y_course..., logp_y_fine)

    # accept or reject
    log_a = logp_y[end] - logp_x[end] - logp_y[end-1] + logp_x[end-1]
    if log_a > log(rand(rng))
        sample = (y, logp_y, (acc_course..., true), n, subchain)
    else
        sample = (x, logp_x, (acc_course..., false), n, subchain)
    end
    
    state = sample[1:2]
    return sample, state 
end


## Ignore subchain in output, just save vector of fine samples 

_unpack_sample(da::MLDA{P,N,false,false,false}, sample::Tuple)  where {P,N} = sample[1]
_unpack_sample(da::MLDA{P,N,true, false,false}, sample::Tuple)  where {P,N} = (sample[1], sample[2][end])
_unpack_sample(da::MLDA{P,N,true, true, false}, sample::Tuple)  where {P,N} = (sample[1], sample[2][end], !sample[3][end])
_unpack_sample(da::MLDA{P,N,false,true, false}, sample::Tuple)  where {P,N} = (sample[1], !sample[3][end])

# Save vector of fine samples
function AbstractMCMC.samples(sample, model::AbstractLogDensity, da::MLDA{P,N,save_logprob,save_reject,false}; kwargs...) where {P,N,save_logprob,save_reject}
    sample = _unpack_sample(da, sample)
    return Vector{typeof(sample)}(undef, 0)
end
function AbstractMCMC.save!!(samples::Vector, sample, ::Integer, model::AbstractLogDensity, da::MLDA{P,N,save_logprob,save_reject,false}; kwargs...) where {P,N,save_logprob,save_reject}
    sample = _unpack_sample(da, sample)
    return BangBang.push!!(samples, sample)
end

# Size hint on sample vector
function AbstractMCMC.samples(sample, model::AbstractLogDensity, da::MLDA{P,L,save_logprob, save_reject, false}, N::Integer; kwargs...) where {P,L,save_logprob, save_reject}
    s = AbstractMCMC.samples(sample, model, da; kwargs...)
    sizehint!(s, N)
    return s
end
function AbstractMCMC.save!!(samples::Vector, sample, iteration::Integer, model::AbstractLogDensity, da::MLDA{P,L,save_logprob, save_reject, false}, N::Integer; kwargs...) where {P,L,save_logprob, save_reject}
    s = AbstractMCMC.save!!(samples, sample, iteration, model, da; kwargs...)
    s !== samples && sizehint!(s,  N)
    return s
end


## Save subchain in output

# unpack a single sample 
_unpack_sample(da::MLDA{P,N,false,false,true}, sample::Tuple) where {P,N} = sample[1]
_unpack_sample(da::MLDA{P,N,true,false,true}, sample::Tuple) where {P,N} = [ zip(sample[1], sample[2])... ]
_unpack_sample(da::MLDA{P,N,false,true,true}, sample::Tuple) where {P,N} = [ zip(sample[1], .! sample[3]) ... ]
_unpack_sample(da::MLDA{P,N,true,true,true}, sample::Tuple) where {P,N} = [ zip(sample[1], sample[2], .! sample[3])... ]

# unpack a sample from a lower level chain
_unpack_subsample(da::MLDA{P,N,false,false,true}, sample::Tuple) where {P,N <: Integer} = map(x->first.(x), sample[5])
_unpack_subsample(da::MLDA{P,N,true,false,true}, sample::Tuple) where {P,N <: Integer} = map(x->getindex.(x, [1:2]), sample[5])
_unpack_subsample(da::MLDA{P,N,false,true,true}, sample::Tuple) where {P,N <: Integer} = map(x->getindex.(x, [1:2:3]), sample[5])
_unpack_subsample(da::MLDA{P,N,true,true,true}, sample::Tuple) where {P,N <: Integer} = sample[5]


# Fixed subchain lengths
function AbstractMCMC.samples(sample::Tuple, model::AbstractLogDensity, da::MLDA{P,N,save_logprob,save_reject,true}; kwargs...) where {P, N<:Integer,save_logprob, save_reject}
    types = typeof.( _unpack_sample(da, sample) )
    samples = [ Vector{T}(undef, 0) for T=types ]
    return samples
end
function AbstractMCMC.save!!(samples::Vector{<:Vector}, sample::Tuple, iteration::Integer, model::AbstractLogDensity, da::MLDA{P,N,save_logprob,save_reject,true}; kwargs...) where {P,N <: Integer,save_logprob,save_reject}
    s = _unpack_sample(da, sample)
    samples .= BangBang.push!!.(samples, s)
    if sample[4]-1 > 0
        s = _unpack_subsample(da, sample)
        samples[1:end-1] .= BangBang.append!!.(samples[1:end-1], s)
    end
    return samples
end

# Size hint for each level 
function AbstractMCMC.samples(sample::Tuple, model::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}, N::Integer; kwargs...) where {P,L <: Integer,save_logprob, save_reject}
    s = AbstractMCMC.samples(sample, model, da; kwargs...)
    sizehint!.(s, size(da) .* N)
    return s
end
function AbstractMCMC.save!!(samples::Vector{<:Vector}, sample::Tuple, iteration::Integer, model::AbstractLogDensity, da::MLDA{P,L,saves_logprob,saves_reject,true}, N::Integer; kwargs...) where {P,L <: Integer,saves_logprob, saves_reject}
    s = AbstractMCMC.save!!(samples, sample, iteration, model, da; kwargs...)
    any(s .!== samples) && sizehint!.(s, size(da) .* N)
    return s
end


# Random subchain lengths
_unpack_sample(da::MLDA{P,L,false,false,true}, sample::Tuple) where {P,L <: Distribution} = [ Tuple.(sample[1]) ... ]
_unpack_subsample(da::MLDA{P,L,false,false,true}, sample::Tuple) where {P,L <: Distribution} = [Tuple.(first.(sample[5][1])), map(x->getindex.(x, [1:3:4]), sample[5][2:end]) ... ]
_unpack_subsample(da::MLDA{P,L,true,false,true}, sample::Tuple) where {P,L <: Distribution} = [map(x->getindex(x,1:2),sample[5][1]), map(x->getindex.(x, [[1,2,4]]), sample[5][2:end]) ... ]
_unpack_subsample(da::MLDA{P,L,false,true,true}, sample::Tuple) where {P,L <: Distribution} = [map(x->getindex(x,1:2:3),sample[5][1]), map(x->getindex.(x, [[1,3,4]]), sample[5][2:end]) ... ]
_unpack_subsample(da::MLDA{P,L,true,true,true}, sample::Tuple) where {P,L <: Distribution} = sample[5]

# save subsample index   
function AbstractMCMC.samples(sample::Tuple, model::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}; kwargs...) where {P,L <: Distribution,save_logprob, save_reject}
    types = typeof.( _unpack_sample(da, sample) )
    types = [ types[1], ( Tuple{t.types ... , Int} for t in types[2:end] ) ... ]  
    samples = [ Vector{T}(undef, 0) for T=types ]
    #println("Initial samples ", typeof.(samples), " ", length.(samples), "\n")
    return samples
end
function AbstractMCMC.save!!(samples::Vector{<:Vector}, sample::Tuple, iteration::Integer, model::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}; kwargs...) where {P,L <: Distribution, save_logprob, save_reject}
    s = _unpack_sample(da, sample)
    s = [s[1], ( (x..., n+1) for (x,n) in zip(s[2:end], length.(samples)[1:end-1]) )... ]
    samples .= BangBang.push!!.(samples, s)

    if sample[4]-1 > 0
        s = _unpack_subsample(da, sample)
        s[2:end] .= [ map( t->(t[1:end-1]..., t[end]+n), x) for (x,n) in zip(s[2:end], length.(samples[1:end-1])) ]
        samples[1] = BangBang.append!!(samples[1], s[1])
        samples[2:end-1] .= BangBang.append!!.(samples[2:end-1], s[2:end])
    end
    return samples
end

# Size hint for each level 
function AbstractMCMC.samples(sample::Tuple, model::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}, N::Integer; kwargs...) where {P,L <: Distribution,save_logprob, save_reject}
    return AbstractMCMC.samples(sample, model, da; kwargs...)
end
function AbstractMCMC.save!!(samples::Vector{<:Vector}, sample::Tuple, iteration::Integer, mode::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}, N::Integer; kwargs...) where {P,L <: Distribution,save_logprob, save_reject}
    return AbstractMCMC.save!!(samples, sample, iteration, mode, da; kwargs...)
end

AbstractMCMC.bundle_samples(samples::Vector{<:Vector}, model::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}, state, ::Type{MultilevelChains}; kwargs...) where {P,L <: Integer,save_logprob,save_reject} = MultilevelChains(samples; fixed_length=true, save_logprob, save_reject)
AbstractMCMC.bundle_samples(samples::Vector{<:Vector}, model::AbstractLogDensity, da::MLDA{P,L,save_logprob,save_reject,true}, state, ::Type{MultilevelChains}; kwargs...) where {P,L <: Distribution,save_logprob,save_reject} = MultilevelChains(samples; fixed_length=false, save_logprob, save_reject)

