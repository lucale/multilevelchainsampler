
using AbstractMCMC
using BangBang


""" 
    MH(p::AbstractProposal; save_logprob=false, save_reject=false)

Metropolis Hastings sampler with proposal `p`. 

- `save_logprob` : saves target evaluations 

- `save_reject`  : saves rejection status
"""
struct MH{P <: AbstractProposal, save_logprob, save_reject} <: AbstractMCMC.AbstractSampler
    proposal :: P
end
MH(p::AbstractProposal, save_logprob::Bool, save_reject::Bool) = MH{typeof(p), save_logprob, save_reject}(p)
MH(p::AbstractProposal; save_logprob=false, save_reject=false) = MH{typeof(p), save_logprob, save_reject}(p)
saves_logprob(mh::MH{P,save_logprob,save_reject}) where {P,save_logprob, save_reject} = save_logprob
saves_reject(mh::MH{P,save_logprob,save_reject}) where {P,save_logprob, save_reject} = save_reject

function AbstractMCMC.step(rng::AbstractRNG, model::AbstractLogDensity, mh::MH; x0=nothing, logp_x0=nothing, kwargs...)

    # start chain from intial state 
    if isnothing(x0) 
        x0 = propose(rng, mh.proposal)
        logp_x0 = evaluate(model, x0)
    elseif isnothing(logp_x0)
        logp_x0 = evaluate(model, x0)
    end

    # update 
    sample = (x0, logp_x0, true)
    state = sample[1:2]
    return sample, state
end

function AbstractMCMC.step(rng::AbstractRNG, model::AbstractLogDensity, mh::MH, state; kwargs...)

    # load previous state
    x, logp_x  = state

    # propose new state
    y = propose(mh.proposal, x)
    logp_y   = evaluate(model, y)
    q_logratio = logpdfratio(mh.proposal, y, x)

    # accept or reject
    log_a = min(0, logp_y - logp_x + q_logratio)
    accept = log(rand(rng)) < log_a

    # update 
    sample = accept ? (y, logp_y, true) : (x, logp_x, false)
    state = sample[1:2]
    return sample, state
end

# dispatch sample unpacking based on save_logprob and save_reject
_unpack_sample(mh::MH{P, false, false}, sample::Tuple) where {P} = sample[1]
_unpack_sample(mh::MH{P, true, false}, sample::Tuple) where {P} = (sample[1], sample[2])
_unpack_sample(mh::MH{P, true, true}, sample::Tuple) where {P} = (sample[1], sample[2], !sample[3])
_unpack_sample(mh::MH{P, false, true}, sample::Tuple) where {P} = (sample[1], !sample[2])

# vector of samples
function AbstractMCMC.samples(sample, model::AbstractLogDensity, mh::MH; kwargs...)
    sample = _unpack_sample(mh, sample) 
    return Vector{typeof(sample)}(undef, 0)
end
function AbstractMCMC.save!!(samples::Vector, sample, ::Integer, model::AbstractLogDensity, mh::MH; kwargs...) 
    sample = _unpack_sample(mh, sample) 
    return BangBang.push!!(samples, sample)
end

# with size hint
function AbstractMCMC.samples(sample, model::AbstractLogDensity, mh::MH, N::Integer; kwargs...)
    s = AbstractMCMC.samples(sample, model, mh; kwargs...)
    sizehint!(s, N)
    return s
end
function AbstractMCMC.save!!(samples::Vector, sample, iteration::Integer, model::AbstractLogDensity, mh::MH, N::Integer; kwargs...) 
    s = AbstractMCMC.save!!(samples, sample, iteration, model, mh; kwargs...)
    s !== samples && sizehint!(s, N)
    return s
end

AbstractMCMC.bundle_samples(samples::Vector, model::AbstractLogDensity, mh::MH, state, chain_type::Type{<:AbstractVector}; kwargs...) = samples
function AbstractMCMC.bundle_samples(samples::Vector, model::AbstractLogDensity, mh::MH, state, chain_type::Type{<:AbstractMCMC.AbstractChains}; kwargs...)
    return chain_type(samples)
end