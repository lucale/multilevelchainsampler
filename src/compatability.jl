#supports_sampler(m::LogDensity, s::MH) = true
#supports_sampler(m::LogDensity{<:Distribution}, s::MLDA) = false
#supports_sampler(m::LogDensity{<:Function}, s::MLDA) = supports_multilevel(m, eltype(s.proposals))
