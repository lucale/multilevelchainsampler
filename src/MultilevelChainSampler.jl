
module MultilevelChainSampler

using AbstractMCMC
using Distributions
using Statistics
using Random
using StatsBase
using LinearAlgebra
using CSV, DataFrames

# proposals
include("proposal.jl")
export propose 
export StaticProposal, RandomWalk
export TransformedProposal
export StackedProposal, ComponentwiseProposal

# log probability density models
include("logdensity.jl")
export LogDensity
export evaluate
export logpdf # reexport from Distributions

# hierarchical output 
include("chains/multichains.jl")
include("chains/statistics.jl")
include("chains/fileoutput.jl")
export save_chain, load_chain
export subchain
export MultilevelChains
export autocor

# chain sampler interface
# implement these functions to support a new sampler

export sample
export supports_sampler
export saves_logprob
export saves_reject
export saves_subchain
export has_fixed_length # whether subchain length is deterministic or not
#export supports_multilevel # whether the sampler supports multilevel chains

# samplers
include("samplers/metropolis_hastings.jl")
include("samplers/delayed_acceptance.jl")
export MH 
export MLDA


# visualization
include("visualize/chainplot.jl")
export chainplot, chainplot!

end