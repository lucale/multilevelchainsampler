using AbstractMCMC
using Distributions
using Random


abstract type AbstractLogDensity <: AbstractMCMC.AbstractModel end

struct LogDensity{T} <: AbstractLogDensity
    density :: T
end

"""
    LogDensity(d::Distribution)
    
Wrap a distribution as a log density model using the `logpdf` function.
"""
LogDensity(d::Distribution) = LogDensity{typeof(d)}(d)
evaluate(m::LogDensity{D}, x) where {D <: Distribution} = logpdf(m.density, x)


"""
    LogDensity(f::Function)

Wrap a function as a unnormalized log density model.
"""
LogDensity(f::Function) = LogDensity{typeof(f)}(f)
evaluate(m::LogDensity{F}, x) where {F <: Function} = m.density(x)

# Function support multilevel method if it has a method for a tuple  
supports_multilevel(m::LogDensity, args...) = false
function supports_multilevel(m::LogDensity{<:Function}, args...)
    return hasmethod(m.density, args) && 
        ( length(args)==1 || supports_multilevel(m.density, args[1:end-1]) )
end