using Makie 

## Plot state for single sample only
function chainplot!(axs::AbstractVector{<:Makie.AbstractAxis}, x::AbstractVector{<:Real}; state_label="State", rename_axes=true)
    ax_time, ax_space = axs[1], axs[2]

    l = lines!(ax_time, 1:length(x), x)
    if rename_axes
        ax_time.xlabel = "Sample"
        ax_time.ylabel = state_label
    end

    d = density!(ax_space, x, color=(:white, 0.0), strokecolor=l.color, strokewidth=l.linewidth)
    if rename_axes
        ax_space.xlabel = state_label
        ax_space.ylabel = "Posterior"
    end

    return [l,d]
end

function chainplot(x::AbstractVector{<:Real}; size=(1024, 512), kwargs...)
    fig = Figure(; size)
    axs = [ Axis(fig[1,1]), Axis(fig[1,2]) ] 
    plt = chainplot!(axs, x; kwargs...)
    return fig, axs, plt 
end 


## For multiple samples
function chainplot!(
    axs::AbstractVector{<:AbstractVector{<:Makie.AbstractAxis}}, x::AbstractVector{<:AbstractVector{<:Real}}; 
    state_label="State", dims=length(x), kwargs...)
    return [
        chainplot!(axs[i], x[i];  
            state_label= (state_label isa String ? "$state_label $i" : state_label[i]), 
            kwargs... 
        )
        for i=1:dims
    ]
end
function chainplot(x::AbstractVector{<:AbstractVector{<:Real}}; dims=length(x), size=(1024, 512 * dims), kwargs...)
    fig = Figure(; size)
    axs = [ [Axis(fig[i,1]), Axis(fig[i,2])] for i=1:dims ]
    plt = chainplot!(axs, x; dims, kwargs...)
    return fig, axs, plt
end 


## with log probability

# single state
function chainplot!(axs::AbstractVector{<:AbstractVector{<:Makie.AbstractAxis}}, x::AbstractVector{<:Real}, logp_x::AbstractVector{<:Real}; kwargs...)
    plt_logp = chainplot!(axs[1], logp_x; state_label="Target")
    plt_x  = chainplot!(axs[2], x; kwargs...)
    return [plt_logp, plt_x] 
end
function chainplot(x::AbstractVector{<:Real}, logp_x::AbstractVector{<:Real}; size=(1024, 512*2), kwargs...)
    fig = Figure(; size)
    axs = [[Axis(fig[i,1]), Axis(fig[i,2])] for i=1:2]
    plts = chainplot!(axs, x, logp_x; kwargs...)
    return fig, axs, plts
end

# multiple states
function chainplot!(
    axs::AbstractVector{<:AbstractVector{<:Makie.AbstractAxis}}, x::AbstractVector{<:AbstractVector{<:Real}}, logp_x::AbstractVector{<:Real}; 
    dims=length(x), kwargs...)

    plt_logp = chainplot!(axs[1], logp_x; state_label="Target")
    plt_x = chainplot!(axs[2:dims+1], x; dims, kwargs...)
    return [plt_logp, plt_x ... ]
end

function chainplot(x::Union{AbstractMatrix{<:Real}, AbstractVector{<:AbstractVector{<:Real}}}, logp_x::AbstractVector{<:Real}; dims=length(x), size=(1024, 512 * (1+dims)), kwargs...)
    fig = Figure(; size)
    axs = [[Axis(fig[i,1]), Axis(fig[i,2])] for i=1:dims+1]
    plts = chainplot!(axs, x, logp_x; dims, kwargs...)
    return fig, axs, plts
end 


## matrix of states
function chainplot!(axs::AbstractVector{<:AbstractVector{<:Makie.AbstractAxis}}, x::AbstractMatrix{<:Real}; dims=size(x,1), kwargs...) 
    return chainplot!(axs, [x[i,:] for i=1:dims]; dims, kwargs...)
end
function chainplot!(axs::AbstractVector{<:AbstractVector{<:Makie.AbstractAxis}}, x::AbstractMatrix{<:Real}, logp_x::AbstractVector{<:Real}; dims=size(x,1), kwargs...) 
    return chainplot!(axs, [x[i,:] for i=1:dims], logp_x; dims, kwargs...)
end
function chainplot(x::AbstractMatrix{<:Real}; dims=size(x,1), kwargs...)
    return chainplot([x[i,:] for i=1:dims]; dims, kwargs...)
end
function chainplot(x::AbstractMatrix{<:Real}, logp_x::AbstractVector{<:Real}; dims=size(x,1), kwargs...)
    return chainplot([x[i,:] for i=1:dims], logp_x; dims, kwargs...)
end

