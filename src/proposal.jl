using Random
using Distributions
import Distributions: logpdf

abstract type AbstractProposal{issymetric} end
SymmetricProposal = AbstractProposal{true}
AsymmetricProposal = AbstractProposal{false}
is_symmetric(p::AbstractProposal{s}) where{s} = s


"""
    propose(p, [x])

Propose a new state from the proposal distribution `p`. If `x` is given, propose a new state from the proposal distribution `p` conditioned on `x`.
"""
function propose end 

propose(p::AbstractProposal) = propose(Random.default_rng(), p)
propose(p::AbstractProposal, x) = propose(Random.default_rng(), p, x)
(p::AbstractProposal)() = propose(p)
(p::AbstractProposal)(x) = propose(p, x)
Base.rand(rng::AbstractRNG, p::AbstractProposal, x...) = propose(rng, p, x... )
Base.rand(p::AbstractProposal, x...) = rand(Random.default_rng(), p, x...)
Base.eltype(p::AbstractProposal) = typeof(propose(p))

# caveat: asymmetric proposals need to implemend logpdfratio or logpdf !
logpdfratio(p::SymmetricProposal, x, y) = 0
logpdfratio(p::AsymmetricProposal, x, y) = logpdf(p, x, y) - logpdf(p, y, x)



""" 
    StaticProposal(d::Distribution)

Indepent proposal from a distribution
"""
struct StaticProposal{D <: Distribution} <: SymmetricProposal
    distribution::D
end
propose(rng::AbstractRNG, p::StaticProposal) = rand(rng, p.distribution)
propose(rng::AbstractRNG, p::StaticProposal, x) = propose(rng, p)
Base.eltype(p::StaticProposal) = eltype(p.distribution)


"""
    RandomWalk(init, step)

Sample from a random walk. The proposal is symmetric if the mean of the step distribution is zero.
"""
struct RandomWalk{issymmetric, I, S} <: AbstractProposal{issymmetric}
    init::I
    step::S
end

# distribution constructors
function RandomWalk(init::Distribution, step::Distribution) 
    issymmetric = sum(abs.(mean(step))) ≈ 0
    RandomWalk{issymmetric, typeof(init), typeof(step)}(init, step)
end
propose(rng::AbstractRNG, p::RandomWalk{issymmetric, <:Distribution, <:Distribution})  where {issymmetric}  = rand(rng, p.init)
propose(rng::AbstractRNG, p::RandomWalk{issymmetric, <:Distribution, <:Distribution}, x) where {issymmetric} = x + rand(rng, p.step)
#logpdf(p::RandomWalk{false, <:Distribution, <:Distribution}, x, y) = logpdf(p.step, y-x)
#TODO: random walk with more complex steps


"""
    TransformedProposal(t, p)

Modify proposal `p` with transformation `t`. The transformation `t` must implement a method for the type of `p`.
"""
struct TransformedProposal{issymmetric, T <: Function, P <: AbstractProposal} <: AbstractProposal{issymmetric}
    trafo::T
    p::P
end
function TransformedProposal(t::Function, p::AbstractProposal) 
    if ! hasmethod(t, (eltype(p),))
        throw(ArgumentError("Transform $t must implement method on $(eltype(p))"))
    end
    # TODO: the transformation might make proposal symmetric if it was not previously
    return TransformedProposal{is_symmetric(p), typeof(t), typeof(p)}(t,p)
end
propose(rng::AbstractRNG, t::TransformedProposal) = t.trafo(propose(rng, t.p))
propose(rng::AbstractRNG, t::TransformedProposal, x) = t.trafo(propose(rng, t.p, x))
#TODO: what about logpdfratio for assymmetric proposals?



"""
    StackedProposal(p...)

Stacked proposal that samples each component independently. The proposal is symmetric if all components are symmetric.
"""
struct StackedProposal{issymmetric, P<:AbstractProposal} <: AbstractProposal{issymmetric} 
    proposals::Vector{P}
end 
function StackedProposal(p::AbstractProposal...) 
    issymmetric = all(is_symmetric, p)
    proposals = [p ... ]; P = eltype(proposals)
    StackedProposal{issymmetric, P}(proposals)
end
Base.length(p::StackedProposal) = length(p.proposals)
Base.getindex(p::StackedProposal, i::Integer) = p.proposals[i]
Base.getindex(p::StackedProposal, idx::UnitRange{<:Integer}) = StackedProposal(p.proposals[idx] ... )
propose(rng::AbstractRNG, p::StackedProposal) = [ propose(rng, p[i]) for i=1:length(p) ]
propose(rng::AbstractRNG, p::StackedProposal, x) = [ propose(rng, p[i], x[i]) for i=1:length(p) ]
logpdfratio(p::StackedProposal{false,P}, x, y) where {P} = sum(logpdfratio(p[i], x[i], y[i]) for i=1:length(p))

Base.stack(p::AbstractProposal...) = StackedProposal(p ... )



## Sample each component independently, like stacked proposal but with different change sizes
"""
    ComponentwiseProposal(p...)

Component-wise proposal that samples each component independently. The proposal is symmetric if all components are symmetric.
Like `StackedProposal`` but with different change sizes for each component.
"""
struct ComponentwiseProposal{issymmetric,P<:AbstractProposal} <: AbstractProposal{issymmetric}
    p::Vector{P}
    n::Int
end

ComponentwiseProposal(p::Vector{<:AbstractProposal}; change_size::Integer=1) = ComponentwiseProposal{all(is_symmetric, p), eltype(p)}(p, change_size)
ComponentwiseProposal(p::AbstractProposal... ; change_size::Integer=1) = ComponentwiseProposal([p ... ]; change_size)
Base.length(c::ComponentwiseProposal) = length(c.p)
Base.getindex(c::ComponentwiseProposal, i::Integer) = c.p[i]
Base.getindex(c::ComponentwiseProposal, idx::UnitRange{<:Integer}) = ComponentwiseProposal(c.p[idx] ... )

propose(rng::AbstractRNG, c::ComponentwiseProposal) = [ propose(rng, c.p[i]) for i=1:length(c) ]
function propose(rng::AbstractRNG, c::ComponentwiseProposal, x) 
    y = copy(x)
    for i=shuffle(rng, 1:length(c))[1:c.n]
        y[i] = propose(rng, c.p[i], x[i])
    end
    return y
end
logpdfratio(c::ComponentwiseProposal{false,P}, x, y) where {P} = sum(logpdfratio(c.p[i], x[i], y[i]) for i=1:length(c))



# convenience functions
#Base.stack(p::AbstractProposal...; change_size::Integer) = ComponentwiseProposal(p... , change_size)
