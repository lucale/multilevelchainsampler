struct MultilevelChains{S <: Vector{<:Vector}, save_logprob, save_reject, fixed_length} <: AbstractMCMC.AbstractChains
    samples::S
end

function MultilevelChains(s::Vector{<:Vector}; save_logprob::Bool=false, save_reject::Bool=false, fixed_length::Bool=false) 
    return MultilevelChains{typeof(s), save_logprob, save_reject, fixed_length}(s)
end
Base.size(c::MultilevelChains) = (length(c.samples) ..., )

# TODO: implement AbstractMCMC.chainscat( ... ) 

function subchain_indices(c::MultilevelChains{S, save_logprob, save_reject, true}, level::Integer) where {S, save_logprob, save_reject} 
    if level == 1 return 1:length(c.samples[1]) end
    shape = length.(c.samples)

    # when discard_initial >= 1
    if all( mod.(shape[1:end-1], shape[2:end]) .== 0 ) 
        N = shape[level]
        ns = StepRange.(1, shape[1:level] .÷ N, shape)
        return ns
    
    # like above except for first sample
    elseif all( mod.(shape[1:end-1] .- 1, shape[2:end] .- 1) .== 0 ) 
        N = shape[level]-1
        ns = StepRange.(2, shape[1:level] .÷ N, shape) 
        ns = vcat.(Base.vect.(ones(Int, level)), collect.(ns))
        return ns
    
    else 
        throw(ErrorException("Subchain lengths $shape are not consistent"))
    end
end


function subchain(c::MultilevelChains{S, save_logprob, save_reject, fixed_length}, level::Integer) where {S,save_logprob, save_reject, fixed_length}
    if level == 1 return c.samples[1] end

    level > length(c.samples)  && throw(ArgumentError("Level $level is larger than maximal level $(length(c.samples))"))

    # get samples 
    ns = subchain_indices(c, level)
    samples = getindex.(c.samples[1:level], ns)
    if fixed_length && ! save_logprob && ! save_reject 
        return [zip(samples... ) ... ]
    end 

    # multilevel states, will always be returned
    x = [ zip(map(t->first.(t), samples)) ... ]  
    
    # log density and rejection flag can be returned optionally
    if save_logprob
        logp_x = getindex.(c.samples[level], 2)
        if ! save_reject return [ zip(x, logp_x) ... ] end
    end
    if save_reject
        reject = getindex.(c.samples[level], 2 + save_logprob)
        if ! save_logp return [ zip(x, reject) ... ] end
    end
    return [ zip(x, logp_x, reject) ... ]
    
end
