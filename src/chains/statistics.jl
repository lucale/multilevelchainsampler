# Description: Statistics functions for chains

# Auto correlation
function autocor(x::Vector, lag::Int, d = x .- mean(x))
    v = sum(d[1+lag:end] .* d[1:end-lag]) 
    c = v / sum(d.^2) * ( 1 - lag / length(x))
    return c
end
function autocor(x::Vector, lag::Union{AbstractRange, Vector{<:Integer}}=1:length(x)-1) 
    d = x .- mean(x)
    return [autocor(x,i,d) for i in lag]
end 
