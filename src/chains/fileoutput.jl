
# Save a chain to a file
function save_chain(filepath::String, x::AbstractVector)
    if endswith(filepath, ".csv")

        if eltype(x) <: Union{AbstractVector, <:NTuple}
            df = DataFrame(x, :auto)
        elseif eltype(x) <: Real
            df = DataFrame([x], [:x])
        else
            @warn "Type of chain elements not recognized. Skipped save."
            return false
        end
        CSV.write(filepath, df)
        return ispath(filepath)
    else 
        @warn "File type of '$filepath' unknown. Skipped save."
        return false
    end
end

# If chain consists of multiple components
function save_chain(filepath::String, x::AbstractVector{<:Union{<:AbstractVector{<:Real}, <:Real}}, logp_x::AbstractVector{<:Real})
    f = split(filepath, ".")
    p = join( vcat( f[1:end-1] , ["state", f[end]] ), "." )
    save_chain(p, x)    

    f = split(filepath, ".")
    p = join( vcat( f[1:end-1] , ["logprob", f[end]] ), "." )
    save_chain(p, logp_x)    
end
#=function save_chain(filepath::String, x::AbstractVector{Union{<:AbstractVector{<:Real}, <:Real}}, logp_x::AbstractVector{<:Real}, accept::Union{BitVector, AbstractVector{Bool}})
    save_chain(filepath, x, logp_x)

    f = split(filepath, ".")
    p = join( vcat( f[1:end-1] , ["accept", f[end]] ), "." )
    save_chain(p, accept)
end
=#


# Load a chain from a file
function load_chain(filepath::String)

    # if file exists load directly 
    if ispath(filepath)
        if endswith(filepath, ".csv")
            df = CSV.read(filepath, DataFrame)

            cols = names(df)
            if length(cols) == 1 
                return df[:, cols[1]]
            else
                return [ Vector(df[:, cols[i]]) for i=1:length(cols) ]
            end
        else
            @warn "File type of '$filepath' unrecognized."
            return missing 
        end        

    # otherwise combination of other files
    else 
        F = split(filepath, ".")
        p = [
            join( vcat( F[1:end-1] , [X, F[end]] ), "." )
            for X in ["state", "logprob", "accept"]
        ]
        f = isfile.(p)
        if all(f) 
            return missing 
        end 

        # load each chain as an array
        l = load_chain.(p[f])
        return length(l) == 1 ? l[1] : l
    end
end