push!(LOAD_PATH,"$(@__DIR__)/../src/")
using Documenter
using MultilevelChainSampler
using Distributions
using Random
using StatsBase
using AbstractMCMC

makedocs(sitename="MultilevelChainSamplers", remotes=nothing)