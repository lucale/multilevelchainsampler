# MultilevelChainSamplers.jl

```@raw html
<h3> Table of contents </h3>
```

```@contents
Depth=5
```

```@raw html
<h3> Table of contents </h3>
```

MCMC Sampler for multilevel delayed acceptance.


## Usage

### Proposal generators

#### List of built-in proposals

```@docs
StaticProposal
```

```@docs
RandomWalk
```

```@docs
TransformedProposal
```

```@docs
StackedProposal
```

```@docs
ComponentwiseProposal
```

#### Custom Proposal generator

To define a custom proposal inherit from the abstract type 
```julia
struct MyProposal <: AbstractProposal{issymmetric::Bool}
    # ... 
end 
```

```@docs
propose
```

Implement the sampling methods 
- initialization x₀ ~ p₀(⋅) by `x₀ = propose(p)`  
- and transition y ~ q(⋅|x) by `y  = propose(p, x)`

```julia
propose(rng::AbstractRNG, p::MyProposal) = ...
propose(rng::AbstractRNG, p::MyProposal, x) = ...
```

For symmetric proposals `q(x|y) = q(y|x)` and the likelihood ratio can be neglected. 
For asymmetric proposals also implement method for calculating  q(y|x)  
```julia
logpdf(p::AbstractProposal{false},x,y) = ...
```


### Sampling 
Implements the `AbstractMCMC` sampling interface. 

```@raw html 
More information can be found on the 
<a href="https://turinglang.org/AbstractMCMC.jl"> AbstractMCMC.jl </a>
documentation page.
```

In a nutshell the sampling function takes the form

```julia
sample(t::LogDensity, mh::Sampler, N_or_isdone, options...)
```

The target `t` is a logarithmic unnormalized probability density 
```@docs
LogDensity
```

In particular the options includes thread and process parallel sampling.



#### Metropolis Hastings (MH)

```@docs
MH
```

Published in [[1]](#1).



#### Multilevel Delayed Acceptance (MLDA)


Construct a multilevel proposal as vector of simple proposals.

```@docs
MLDA
```

Published in [[1]](#2).


## Plotting 

Based on Makie.

Plot the trajectory of a sampled chain.
```julia
chainplot(x [, logp_x] ; ... )
chainplot!(axs, x [, logp_x] ; ...)
```



## Reference

### Index 

```@index
```

### Citations 

```@raw html
<a id="1">[1]</a> 
Hastings, W.K. (1970). 
"Monte Carlo Sampling Methods Using Markov Chains and Their Applications". 
Biometrika, Volume 57, Issue 1

<br />

<a id="2">[2]</a>
Mikkel B. Lykkegaard, T. Dodwell, C. Fox, Grigorios Mingas, Robert Scheichl (2022). 
"Multilevel Delayed Acceptance MCMC"
SIAM/ASA J. Uncertain. Quantification
```




