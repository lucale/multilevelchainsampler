using Test
using Distributions
using Statistics
using AbstractMCMC

using Revise
using MultilevelChainSampler



@testset "hastings" begin
    # behaves like truncation on cyclic unit interval 
    t = LogDensity(Normal(0,1))
    p = StaticProposal(Uniform(-1, 1))

    # ignore log probability
    mh = MH(p)
    c = sample(t, mh, 100000)
    @test length(c) == 100000
    @test typeof(c[1]) == Float64

    # sample multiple chains at once
    c = sample(t, mh, AbstractMCMC.MCMCSerial(), 1000, 3)
    @test length(c) == 3
    @test all(length.(c) .== 1000)
    @test typeof(c[1][1]) == Float64

    # save logprob and rejection 
    mh = MH(p, true, true)
    c = sample(t, mh, 1000)
    @test length(c[1]) == 3
    @test length(getindex.(c,1))==1000 
    @test typeof(c[1]) == Tuple{Float64, Float64, Bool}


    t = LogDensity(x->-sum(x.^2))
    p = StaticProposal(Normal(0,1))
    p = stack(p,p)
    mh = MH(p, save_logprob = true)
    sample(t, mh, 100)
end


@testset "delayed_acceptance" begin

    p = StaticProposal(Normal(0,1))
    t = LogDensity(x->-sum(x.^2))

    # sample from two level, ignore subchains
    da = MLDA([p,p], [2,])
    c = sample(t, da, 100)
    @test length(c) == 100
    @test length(c[1]) == 2
    @test typeof(c[1]) == NTuple{2,Float64}

    
    da = MLDA([p,p], [3,]; save_logprob=true)
    c = sample(t, da, 100)
    @test length(c) == 100
    @test length(c[1]) == 2
    @test typeof(c[1]) == Tuple{Tuple{Float64, Float64}, Float64}

    
    # save subchains
    da = MLDA([p,p], [3,]; save_subchain=true)
    c = sample(t, da, 100; discard_initial=1)
    @test typeof(c) <: Vector{<:Vector}
    @test all( length.(c) .== 100 .* size(da) )
    

    # sample from three level
    da = MLDA([p,p,p], [2,3]; save_subchain=true)
    c = sample(t, da, 100, discard_initial=1)
    @test typeof(c) <: Vector{<:Vector}
    @test all( length.(c) .== 100 .* size(da) )


    da = MLDA([p,p,p], [3,2]; save_reject=true, save_subchain=true)
    c = sample(t, da, 100, discard_initial=1)
    
    @test length(c) == 3
    @test all( length.(c) .== 100 .* size(da) )
    @test all( typeof.(c[1]) .== [ Tuple{Float64, Bool } ] )
    
    x1 = first.(c[1][1:6:end]) 
    x2 = first.(c[2][1:2:end])
    x3 = first.(c[3][1:1:end])
    x = [ zip(x1,x2,x3) ... ]
    rej_rates = mean.(map(t->last.(t), c))
    #println("Rejection rates ", rej_rates)

    last_state = last.(c); 
    x = first.(last_state)
    @test all( eltype.(c) .== Tuple{Float64,Bool} )

    
    # random subchain length
    d = truncated(Poisson(), 1, nothing)
    da = MLDA([p,p], [d,]; save_subchain=true) 
    c = sample(t, da, 100, discard_initial=1)
    @test all( typeof.(c) == [ Vector{Tuple{Float64}}, Vector{Tuple{Float64, Int}}] )
    @test length(c[2]) == 100
    @test all( unique( last.(c[2]) ) .== last.(c[2]) ) 

    d = truncated(Poisson(), 1, nothing)
    da = MLDA([p,p], [d,]; save_logprob=true, save_subchain=true) 
    c = sample(t, da, 100, discard_initial=1)
    @test all( typeof.(c) == [ Vector{Tuple{Float64,Float64}}, Vector{Tuple{Float64, Float64, Int}}] )
    @test length(c[2]) == 100
    @test all( unique( last.(c[2]) ) .== last.(c[2]) ) 

    d = truncated(Poisson(), 1, nothing)
    da = MLDA([p,p], [d,]; save_reject=true, save_subchain=true) 
    c = sample(t, da, 100, discard_initial=1)
    @test all( typeof.(c) == [ Vector{Tuple{Float64,Bool}}, Vector{Tuple{Float64, Bool, Int}}] )
    @test length(c[2]) == 100
    @test issorted(last.(c[2]))
    @test all( unique( last.(c[2]) ) .== last.(c[2]) ) 


    d = truncated(Poisson(), 1, nothing)
    da = MLDA([p,p,p], [d,d]; save_reject=true, save_subchain=true) 
    c = sample(t, da, 100, discard_initial=1)
    @test all( typeof.(c) == [ Vector{Tuple{Float64,Bool}}, Vector{Tuple{Float64,Bool, Int}}, Vector{Tuple{Float64, Bool, Int}}] )
    @test length(c[3]) == 100
    @test issorted(last.(c[2])) && issorted(last.(c[3]))
    @test all( unique( last.(c[2]) ) .== last.(c[2]) ) && all( unique( last.(c[3]) ) .== last.(c[3]) )

end

