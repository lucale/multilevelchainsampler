
using Test
using Distributions
using Statistics
using AbstractMCMC

using Revise
using MultilevelChainSampler

@testset "proposals" begin
    function propose_chain(p, n=100)
        x = [propose(p)] 
        for i in 1:n
            push!(x, propose(p, x[end]))
        end
        return x
    end
    
    # sample from a normal distribution
    p = StaticProposal(Normal(0,1)) 
    c = propose_chain(p, 10000)
    @test ≈( mean(c), 0.0 , atol=0.1 )
    @test ≈( std(c),  1.0 , atol=0.1 )


    # multi proposal
    p = stack(
        StaticProposal(Normal(0,1)),
        StaticProposal(Normal(0,1))
    )
    c = propose_chain(p, 10000)
    @test ≈( mean(c) , [0.0, 0.0] , atol=0.1 )
    @test ≈( std(c) , [1.0, 1.0] , atol=0.1 )
end


@testset "hastings" begin

    # behaves like truncation on cyclic unit interval 
    t = LogDensity(x->-sum(x.^2 /2))
    cycle(a,b) = t -> @. mod(t-a, b-a) + a
    CyclicWalk(a,b,s) = TransformedProposal(cycle(a,b), RandomWalk(Uniform(a,b), Uniform(-s,s)))
    p = CyclicWalk(-1, 1, .25)

    # save log probability
    mh = MH(p, false, false)
    x = sample(t, mh, 100000)
    @test length(x) == 100000
    @test ≈( mean(x), 0.0 , atol=0.1 )
    @test ≈( std(x),  std(TruncatedNormal(0,1,-1,1)) , atol=0.1 )

end


# TODO: write test for delayed acceptance
