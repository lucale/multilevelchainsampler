using Revise


using Test
using Random
using MultilevelChainSampler
using Distributions

using CairoMakie

Random.seed!(1234)

d = Normal(0,1)
x1 = rand(d, 1000)
x2 = rand(d, 1000)
y1 = rand(d, 3, 1000)
y2 = rand(d, 5, 1000)
logp_x1 = logpdf.([d], x1)
logp_x2 = logpdf.([d], x2)
logp_y1 = logpdf.([d], y1[1,:]) + logpdf.([d], y1[2,:])
logp_y2 = logpdf.([d], y2[1,:]) + logpdf.([d], y2[2,:])

fig,ax,plt = chainplot(x1) 
chainplot!(ax, x2)
display(fig)

fig,ax,plt = chainplot(x1, logp_x1)
chainplot!(ax, x2, logp_x2)
display(fig)

fig,ax,plt = chainplot(y1)
chainplot!(ax, y2, dims=3)
display(fig)

fig,ax,plt = chainplot(y1, logp_y1, dims=2)
chainplot!(ax, y2, logp_y2, dims=2)
display(fig)