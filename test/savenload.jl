using Test
using Distributions
using Statistics
using AbstractMCMC
using LinearAlgebra

using Revise
using MultilevelChainSampler


#@testset "hastings" begin

@testset "1D" begin
    P = Normal(0, 1)
    t = LogDensity(P)
    p = StaticProposal(P)

    mh = MH(p)
    c = sample(t, mh, 1000)
    save_chain("test1D.csv", c)
    c2 = load_chain("test1D.csv")
    @test c == c2
    rm("test1D.csv")

    mh = MH(p, save_logprob=true)
    c = sample(t, mh, 1000)
    x = getindex.(c, 1)
    logp_x = getindex.(c, 2)
    save_chain("test1D.csv", x, logp_x)
    c2 = load_chain("test1D.csv")
    @test c2[1] == x
    @test c2[2] == logp_x

    for f in readdir()
        startswith(f, "test1D") && rm(f)
    end
end 

@testset "2D" begin
    P = MvNormal( zeros(10), diagm(ones(10)))
    t = LogDensity(P)
    p = StaticProposal(P)

    mh = MH(p)
    c = sample(t, mh, 1000)
    save_chain("test2D.csv", c)
    c2 = load_chain("test2D.csv")
    @test c == c2
    rm("test2D.csv")


    mh = MH(p, save_logprob=true)
    c = sample(t, mh, 1000)
    x = getindex.(c, 1)
    logp_x = getindex.(c, 2)
    save_chain("test2D.csv", x, logp_x)
    c2 = load_chain("test2D.csv")
    @test c2[1] == x
    @test c2[2] == logp_x

    for f in readdir()
        startswith(f, "test2D") && rm(f)
    end

end