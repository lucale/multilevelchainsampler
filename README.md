# MultilevelChainSampler.jl

Sampling a chain from a limit of proxy distributions. 
This is a generalization of Metropolis Hastings type sampling algorithm.

Implements following sampling algorithms:
- Metropolis-Hastings algorithm (MH) [[1]](#1)
- Multilevel Delayed Acceptance (MLDA) [[2]](#2)

 
## Documentation 

The documentation is automatically built. You can find it under `docs/build/index.html`.

To build it yourself run 
`julia --color=yes --project docs/make.jl`

and to view it 
`julia -e 'using LiveServer; serve(dir="docs/build")' `


## References 

<a id="1">[1]</a> 
Hastings, W.K. (1970). 
"Monte Carlo Sampling Methods Using Markov Chains and Their Applications". 
Biometrika, Volume 57, Issue 1
 
<a id="2">[2]</a>
Mikkel B. Lykkegaard, T. Dodwell, C. Fox, Grigorios Mingas, Robert Scheichl (2022). 
"Multilevel Delayed Acceptance MCMC"
SIAM/ASA J. Uncertain. Quantification
